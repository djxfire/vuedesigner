function regAjaxForm() {
    $(document).on('submit','form.ajax',(e) => {
        e.preventDefault()
        let $this = $(e.target)
        let success = $this.attr('on-success')
        let error = $this.attr('on-error')
        let before = $this.attr('on-before')
        if(before) {
            let before_res = new Function('e', before + '(e)')(e)
            if(before_res === false) {
                return false
            }
        }
        $.ajax({
            type : $this.attr('method'),
            url : $this.attr('action'),
            data : $this.serialize(),
            success : (msg) => {
                new Function('e', 'msg', success + '(e,msg)')(e,msg)
            },
            error: (err) => {
                new Function('e', 'msg', error + '(e,msg)')(e ,err)
            }
        })
        return false
    })
}
function regAuthAjaxForm() {
    $(document).on('submit','form.auth-ajax',(e) => {
        e.preventDefault()
        let $this = $(e.target)
        let success = $this.attr('on-success')
        let error = $this.attr('on-error')
        let before = $this.attr('on-before')
        if(before) {
            let before_res = new Function('e', before + '(e)')(e)
            if(before_res === false) {
                return false
            }
        }
        $.ajax({
            type : $this.attr('method'),
            url : $this.attr('action'),
            data : $this.serialize(),
            success : (msg) => {
                if(msg.code === -1001) {
                    alert('您还未登录，请先登录')
                    window.location.href = '/user/login'
                }else {
                    new Function('e', 'msg', success + '(e,msg)')(e,msg)
                }

            },
            error: (err) => {
                new Function('e', 'msg', error + '(e,msg)')(e ,err)
            }
        })
        return false
    })
}
function refresh_captcha() {
    $(".captcha").attr('src','/captcha?' + Math.random())
}
function regCaptchaClick() {
    $(".captcha").click(function(res){
        refresh_captcha()
    })
}
function alert_success(msg) {
    toast('success', msg, 2000)
}
function alert_danger(msg) {
    toast('danger', msg, 2000)
}
function alert_info(msg) {
    toast('info', msg, 2000)
}
function alert_warning(msg) {
    toast('warning', msg, 2000)
}
function stamp2date(inputTime) {
    let date = new Date(inputTime)
    let y = date.getFullYear()
    let m = date.getMonth() + 1
    m = m < 10 ? ('0' + m) : m
    let d = date.getDate()
    d = d < 10 ? ('0' + d) : d
    let h = date.getHours()
    h = h < 10 ? ('0' + h) : h
    let minute = date.getMinutes()
    let second = date.getSeconds()
    minute = minute < 10 ? ('0' + minute) : minute
    second = second < 10 ? ('0' + second) : second
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second
}
function toast(type,message,time) {
    let dv = document.createElement("div")
    dv.style.position = "absolute"
    if(type === "success") {
        dv.style.background = "#66CD00"
    }else if(type === "info") {
        dv.style.background = "#87CEFA"
    }else if(type === "warning") {
        dv.style.background = "#FF8C00"
    }else if(type === "danger") {
        dv.style.background = "#FF3030"
    }else {
        dv.style.background = "#333"
    }

    dv.style.opacity = "0.8"
    dv.style.color = "#FFF"
    dv.style.margin = "auto"
    dv.style.top = "0"
    dv.style.bottom = "0"
    dv.style.left = "0"
    dv.style.right = "0"
    dv.style.display="inline"
    dv.style.fontSize="16px"
    dv.style.padding= "8px"
    dv.style.width = (16*message.length + 16) + "px"
    dv.style.height = "36px"
    dv.style.textAlign = "center"
    dv.style.zIndex = "996"
    dv.innerHTML = message
    document.body.appendChild(dv)
    setTimeout(()=>{
        document.body.removeChild(dv)
    },time)
}
$(function (){
    regAjaxForm()
    regAuthAjaxForm()
    regCaptchaClick()
})
