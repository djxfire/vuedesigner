/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : vuedesigner

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-10-09 18:05:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ve_mobile_project`
-- ----------------------------
DROP TABLE IF EXISTS `ve_mobile_project`;
CREATE TABLE `ve_mobile_project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(64) NOT NULL COMMENT '工程名称',
  `content` blob COMMENT '生成内容',
  `origin` blob COMMENT '编辑内容',
  `createtime` int(11) NOT NULL,
  `agent` varchar(64) DEFAULT NULL COMMENT '代理地址',
  `is_agent` smallint(4) NOT NULL DEFAULT '0' COMMENT '是否使用代理',
  `remote` int(11) DEFAULT NULL COMMENT '远程工程',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ve_mobile_project
-- ----------------------------

-- ----------------------------
-- Table structure for `ve_user`
-- ----------------------------
DROP TABLE IF EXISTS `ve_user`;
CREATE TABLE `ve_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `regtime` int(10) NOT NULL,
  `is_active` smallint(4) NOT NULL,
  `active_code` varchar(64) NOT NULL,
  `active_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ve_user
-- ----------------------------
