const {JSON,D,FETCH} = require('../fastfn')
const {db,m} = require('../model/m')
const Query = require('../model/Query')
const {decorate_login} = require('../core')
const {stamp2date} = require('../common')
const qr = require('qr-image')
exports.list = function *(ctx){
    let islogin = decorate_login(ctx)
    if(islogin !== true) {
        if(islogin === undefined){
            return
        }
        return JSON(islogin)
    }
    yield D('isLogin',true)
    yield D('stamp2date',stamp2date)
    yield db(async (conn) => {
        let query = new Query(conn,'ve_mobile_project')
        try{
            let page = ctx.query.page?ctx.query.page:1
            let list = await query.where('uid',ctx.session.userid).page(page,6)
            let count = await m(conn,"SELECT count(1) as cnt FROM ve_mobile_project WHERE uid = '?'",ctx.session.userid)
            return D('projects',{
                cur:page,
                all:count[0].cnt,
                pageSize:6,
                pages:Math.ceil(count[0].cnt/6),
                list:list
            })
        }catch(err) {
            console.log(err)
            return D('projects',{
                cur:1,
                all:0,
                pageSize:6,
                pages:0,
                data:[]
            })
        }
    })
    yield FETCH()
}
exports.add = function *(ctx) {
    let islogin = decorate_login(ctx)
    if(islogin !== true) {
        if(islogin === undefined){
            return
        }
        return JSON(islogin)
    }
    yield db(async (conn) => {
        let query = new Query(conn,'ve_mobile_project')
        //构建project Model
        let project = {
            uid: ctx.session.userid,
            name:ctx.request.body.name,
            description:ctx.request.body.description,
            createtime:new Date().getTime()
        }
        try{
            let res = await query.insert(project)
            return JSON({code:0, msg:'新建项目成功'})
        }catch(err) {
            console.log(err)
            return JSON({code:1, msg:'新建项目失败，请重试'})
        }
    })
}

exports.edit = function *(ctx) {
    let islogin = decorate_login(ctx)
    if(islogin !== true) {
        if(islogin === undefined){
            return
        }
        return JSON(islogin)
    }
    let projectid = ctx.query.id
    ctx.session.projectid = projectid
    yield FETCH()
}

exports.init = function *(ctx) {
    let islogin = decorate_login(ctx)
    if(islogin !== true) {
        if(islogin === undefined){
            return
        }
        return JSON(islogin)
    }
    let projectid = ctx.session.projectid
    yield db(async (conn) => {
        let query = new Query(conn, 've_mobile_project')
        let project = await query.where('id', projectid).find()
        return D('project',project)
    })
    yield JSON({code:0, msg:'获取工程成功'})
}
exports.design = function *(ctx) {
    let islogin = decorate_login(ctx)
    if(islogin !== true) {
        if(islogin === undefined){
            return
        }
        return JSON(islogin)
    }
    let projectid = ctx.session.projectid
    let origin = ctx.request.body.origin
    let content = ctx.request.body.content
    yield db(async (conn) => {
        let query = new Query(conn, 've_mobile_project')
        let res = await query.where("id",projectid).update({
            origin:origin,
            content:content
        })
        if(res) {
            return JSON({code:0,msg:'保存成功'})
        }else {
            return JSON({code:1,msg:res})
        }
    })
}

exports.preview = function *(ctx) {
    let projectid = ctx.session.projectid
    console.log(ctx.request)
    try{
        let img = qr.image('http://' + ctx.request.hostname + ':' + ctx.request.port + '/project/view?id=' + projectid,{size:10} )
        ctx.body = img
        ctx.type = 'image/png'
        yield FETCH()
    }catch(err){
        console.log(err)
        ctx.body = 0
        ctx.type = 'image/png'
        yield FETCH()
    }

}