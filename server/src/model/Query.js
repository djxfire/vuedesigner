module.exports = class {
    constructor (conn, name) {
        this.conn = conn
        this.name = name
        this.joins = []
        this.wheres = []
    }
    alias(al) {
        this.al = al
        return this
    }
    fields (fields) {
        this.fields_str = fields
        return this
    }
    where (...where) {
        this.wheres.push(where)
        return this
    }
    join (...join) {
        this.joins.push({
            table: join[0],
            on: join[1],
            relation: join[2]?join[2]:'INNER'
        })
        return this
    }
    order (order) {
        this.order_str = order
        return this
    }
    group (group) {
        this.group_str = group
    }
    limit (limit) {
        this.limit_str = limit
        return this
    }
    reset() {
        this.joins.splice(0,this.joins.length)
        this.wheres.splice(0,this.wheres.length)
        this.order_str = undefined
        this.group_str = undefined
        this.limit_str = undefined
        this.fields_str = undefined
        this.al = undefined
    }
    page (page, pageSize) {
        let select_sql = 'SELECT ' + (this.fields_str?this.fields_str:' * ')
        + ' FROM ' + this.name + ' ' + (this.al?' as ' + this.al : '') + ' '
        let join_sql = ''
        if(this.joins) {
            for(let join of this.joins) {
                join_sql += join.relation + ' JOIN ' + join.table + ' ON ' + join.on
            }
        }
        select_sql += join_sql
        select_sql += ' WHERE 1 = 1 ' + this.get_where(this.wheres)
        select_sql += this.order_str?' ORDER BY ' + this.order_str:''
        select_sql += this.group_str?' GROUP BY ' + this.group_str:''
        select_sql += ' LIMIT ' + (pageSize*(page-1)) + ',' + pageSize
        this.reset()
        return new Promise((resolve, reject) => {
            this.conn.query(select_sql, [], (err, result) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(result)
                }
            })
        })
    }
    buildSql() {
        let select_sql = 'SELECT ' + (this.fields_str?this.fields_str:' * ')
            + ' FROM ' + this.name +  ' ' +  (this.al?this.al:'') + ' '
        let join_sql = ''
        if (this.joins) {
            for (let join of this.joins) {
                join_sql += join.relation + ' JOIN ' + join.table + ' ON ' + join.on
            }
        }
        select_sql += join_sql
        select_sql += ' WHERE 1 = 1 ' + this.get_where(this.wheres)
        select_sql += this.order_str?' ORDER BY ' + this.order_str:''
        select_sql += this.group_str?' GROUP BY ' + this.group_str:''
        select_sql += this.limit_str?' LIMIT ' + this.limit_str:''
        this.reset()
        return '(' + select_sql + ')'
    }
    select() {
        let select_sql = 'SELECT ' + (this.fields_str?this.fields_str:' * ')
            + ' FROM ' + this.name + ' ' +  (this.al?this.al:'') + ' '
        let join_sql = ''
        if (this.joins) {
            for (let join of this.joins) {
                join_sql += join.relation + ' JOIN ' + join.table + ' ON ' + join.on
            }
        }
        select_sql += join_sql
        select_sql += ' WHERE 1 = 1 ' + this.get_where(this.wheres)
        select_sql += this.order_str?' ORDER BY ' + this.order_str:''
        select_sql += this.group_str?' GROUP BY ' + this.group_str:''
        select_sql += this.limit_str?' LIMIT ' + this.limit_str:''
        this.reset()
        return new Promise((resolve,reject) => {
            this.conn.query(select_sql, [],(err, result) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(result)
                }
            })
        })
    }
    delete() {
        let delete_sql = 'DELETE FORM ' + this.name + ' ' + this.get_where(this.wheres)
        this.reset()
        return new Promise((resolve, reject) => {
            this.conn.query(delete_sql, [], (err, result) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(result)
                }
            })
        })
    }
    find() {
        let select_sql = 'SELECT ' + (this.fields_str?this.fields_str:' * ')
            + ' FROM ' + this.name + ' ' +  (this.al?this.al:'') + ' '
        let join_sql = ''
        if (this.joins) {
            for (let join of this.joins) {
                join_sql += join.relation + ' JOIN ' + join.table + ' ON ' + join.on
            }
        }
        select_sql += join_sql
        select_sql += ' WHERE 1 = 1 ' + this.get_where(this.wheres)
        select_sql += this.order_str?' ORDER BY ' + this.order_str:''
        select_sql += this.group_str?' GROUP BY ' + this.group_str:''
        select_sql += ' LIMIT  1'
        this.reset()
        return new Promise((resolve,reject) => {
            this.conn.query(select_sql, [], (err,result) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(result.length > 0?result[0]:undefined)
                }
            })
        })
    }
    insert(obj) {
        let {sql, values} = this.get_insert_statement(obj)
        this.reset()
        return new Promise((resolve, reject) => {
            this.conn.query(sql, values, (err, result) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(result)
                }
            })
        })
    }

    update(obj) {
        let update_sql = this.get_update_statement(obj,this.wheres)
        console.log(update_sql)
        this.reset()
        return new Promise((resolve, reject) => {
            this.conn.query(update_sql, [], (err, result) => {
                if(err) {
                    reject(err)
                }else {
                    resolve(result)
                }
            })
        })
    }
    get_update_statement (obj, wheres) {
        let ret = 'UPDATE ' + this.name + ' SET '
        for (let field in obj) {
            ret += field + ' = \'' + obj[field] + '\','
        }
        ret = ret.substr(0, ret.length - 1)
        ret +=  ' WHERE 1 = 1 '
        ret += this.get_where(wheres)
        return ret
    }
    get_insert_statement (obj) {
        let sql = 'INSERT INTO ' + this.name,
            field_arr = [],
            tmp = [],
            values = []
        for (let key in obj) {
            field_arr.push(key)
            tmp.push('?')
            values.push(obj[key])
        }
        sql += '( ' + field_arr.join(',') + ') VALUES (' + tmp.join(',') + ')'
        return { sql, values }
    }

    get_where (wheres) {
        let ret = ''
        for (let where of wheres) {
            if (!where instanceof Array) {
                ret += ' AND ' + where
            }else {
                if (where[0].trim() == 'and' || where[0].trim() == 'or') {
                    if (where.length == 2) {
                        ret += ' ' + where[0] + ' ' + where[1]
                    }else if (where.length == 3) {
                        ret += ' ' + where[0] + ' ' + where[1] + ' = \'' + where[2] + '\''
                    }else if (where.length == 4) {
                        ret += ' ' + where[0] + ' ' + where[1] + ' ' + where[2] + ' ' + '\'' + where[3] + '\''
                    }
                }else if (where.length == 2) {
                    ret += ' AND ' + where[0] + ' = \'' + where[1] + '\''
                }else if (where.length == 3) {
                    ret += 'AND ' + where[0] + ' ' + where[1] + ' \'' + where[2] + '\''
                }
            }
        }
        return ret
    }
}