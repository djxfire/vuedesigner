const mysql = require('mysql')
const config = require('./config')
let pool = mysql.createPool(config)
exports.db = (_m) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, conn) => {
            if(err) {
                reject(err)
            }else {
                let promise = _m(conn)
                promise.then(result => {
                    resolve(result)
                }).catch(err => {
                    reject(err)
                })
            }
        })
    })
}
exports.tran = (tran) => {
    return new Promise((resolve, reject) => {  //返回promise提供事务成功与失败的接口
        pool.getConnection((err, conn) => {
            if(err) {
                reject(err)
            }else {
                conn.beginTransaction((err) => { //开始事务处理
                    if(err) {
                        conn.release()
                        reject(err)
                    }else {
                        let promise = tran(conn)  //调用事务处理函数
                        promise.then(result => {
                            conn.commit(err => {  //事务处理函数resolve则提交事务
                                if(err) {
                                    reject(err)
                                }else {
                                    resolve(result)
                                }
                            })
                        }).catch(err => {
                            conn.rollback(() => {  //事务处理函数reject则回滚事务
                                conn.release()
                                reject(err)
                            })
                        })
                    }
                })
            }
        })
    })
}
exports.m = function() {
    if(arguments.length < 2){
        return Promise.reject('参数错误')
    }
    let conn = arguments[0]
    let sql = arguments[1]
    let args = []
    for(let i = 2;i < arguments.length;i++) {
        args.push(arguments[i])
    }
    return new Promise((resolve,reject) => {
        conn.query(sql, args, (err, result) => {
            if(err) {
                reject(err)
            }else {
                resolve(result)
            }
        })
    })
}