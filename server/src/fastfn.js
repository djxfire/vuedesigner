/**
 * 设置Header的快捷键
 * @param key
 * @param value
 * @constructor
 */
const _HEADER = 0
const _DATA = 1
const _RET_TYPE = 2
const _REDIRECT = 3 //跳转
const _COOKIE = 4

exports.H = (key, value) => {
    return {
        opts : _HEADER,
        key : key,
        value : value
    }
}
exports.D = (key, value) => {
    return {
        opts : _DATA,
        key : key,
        value : value
    }
}
exports.JSON = (obj) => {
    return {
        opts : _RET_TYPE,
        key : 'json',
        value : obj
    }
}
exports.FETCH = (obj) => {
    return {
        opts : _RET_TYPE,
        key : 'fetch',
        value : obj
    }
}
exports.C = (key, value, options) => {
    return {
        opts : _COOKIE,
        key : key,
        value : value,
        options : options
    }
}

exports.U = (url) => {
    return {
        opts : _REDIRECT,
        key : 'url',
        value : url
    }
}