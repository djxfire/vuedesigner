module.exports.randstr = () => {
    return Math.random().toString(36).substr(2)
}
module.exports.isGeneratorFunction = obj => {
    let constructor = obj.constructor
    if(!constructor) return false
    if('GeneratorFunction' === constructor.name || 'GeneratorFunction' === constructor.displayName) return true
    return 'function' == typeof obj.next && 'function' == typeof obj.throw()
}

module.exports.isResponseCode = _var => {
    if(!/^[2-5][0-9]{2}$/.test(_var)) {
        return false
    }
    return true
}
module.exports.isArray = _var => {
    if(typeof _var === 'object' && Object.prototype.toString.call(_var) === '[object Array]') {
        return true
    }
    return false
}
module.exports.isObject = _var => {
    if(typeof _var === 'object' && Object.prototype.toString.call(_var) === '[object Object]') {
        return true
    }
    return false
}
module.exports.isPromise = _var => {
    if(typeof _var === 'object' && Object.prototype.toString.call(_var) === '[object Promise]') {
        return true
    }
    return false
}
module.exports.stamp2date = (inputTime) => {
    let date = new Date(inputTime)
    let y = date.getFullYear()
    let m = date.getMonth() + 1
    m = m < 10 ? ('0' + m) : m
    let d = date.getDate()
    d = d < 10 ? ('0' + d) : d
    let h = date.getHours()
    h = h < 10 ? ('0' + h) : h
    let minute = date.getMinutes()
    let second = date.getSeconds()
    minute = minute < 10 ? ('0' + minute) : minute
    second = second < 10 ? ('0' + second) : second
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second
}