let config = require('../config')
exports.SERVER = config.host + ':' + config.port
exports.check_captcha = (ctx,captcha) => {
    if(ctx.session.captcha === captcha) {
        ctx.session.captcha == undefined
        return true
    }else {
        ctx.session.captcha == undefined
        return  false
    }
}
exports.decorate_login = (ctx, next) => {
    if(ctx.session.username !== undefined) {
        return true
    }
    if(ctx.req.headers['x-requested-with']&&ctx.req.headers['x-requested-with'].toLowerCase() == 'xmlhttprequest'){
        return {code:-1001,msg: '您还未登录'}
    }else {
        ctx.redirect('/user/login')
    }
}
