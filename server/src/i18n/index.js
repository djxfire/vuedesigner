const requireDirectory = require('require-directory')
const config = require('../../config')
let i18n = requireDirectory(module)
module.exports = config.lang && i18n[config.lang]?i18n[config.lang]:i18n['cn']