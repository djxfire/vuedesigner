const ccap = require('svg-captcha')
module.exports = async (ctx, next) => {
    let path = ctx.request.path
    if(path === '/captcha') {
        let ary = ccap.create({fontSize:48,width:100})
        ctx.body = ary.data
        ctx.type = 'image/svg+xml'
        ctx.session.captcha = ary.text
    }else {
        await next()
    }
}