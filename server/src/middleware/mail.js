let mailer = require('nodemailer')
let config = require('../../config')

let transporter = mailer.createTransport({
    service: config.mail.server,
    auth: config.mail.auth
})
module.exports = function() {
    let send_mail = function(to, subject ,content) {
        var option = {
            from:config.mail.auth.user,//发送邮件的邮箱
            to:to //目标邮箱
        }
        option.subject = subject
        option.html= content
        return new Promise((resolve, reject) => {
            transporter.sendMail(option, function(error, response){
                if(error){
                    reject(error)
                }else{
                    resolve(response)
                }
            })
        })
    }
    return function(ctx, next) {
        ctx.send_mail = send_mail
        return next()
    }

}