module.exports = async (ctx, next) => {
    let path = ctx.request.path
    if(path === '/mail_auth') {
        let ary0 = Math.floor(Math.random()*10),
            ary1 = Math.floor(Math.random()*10),
            ary2 = Math.floor(Math.random()*10),
            ary3 = Math.floor(Math.random()*10)
        let ary = ary0 + '' + ary1 + '' + ary2 + '' +ary3
        let email = ctx.query.email
        try{
            let res = await ctx.send_mail(email,'激活码','欢迎注册VUEDesigner,您的激活码：' + ary)
            ctx.body = JSON.stringify({code:0,msg:'发送成功'})
            ctx.type = 'json'
            ctx.session.mail_auth = ary
        }catch(err) {
            ctx.body = '{code:0,msg:"发送失败,请重试"}'
            ctx.type = 'json'
        }
    }else {
        await next()
    }
}