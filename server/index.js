'use strict'
//require("babel-core/register")
const koa = require('koa')
const session = require("koa-session")
const path = require('path')
const config = require('./config')
const router = require('./src/routes')
const xtpl = require('koa-xtpl')
const koaStatic = require('koa-static')
const koaBody = require('koa-body')
const ccap = require('./src/middleware/ccap')
const mail = require('./src/middleware/mail')
const auth_mail = require('./src/middleware/auth_mail')
let app = new koa()

app.keys = config.app_key
app.use(session(config.session, app))
app.use(ccap)
app.use(mail())
app.use(auth_mail)
app.use(xtpl({
    root:path.join(__dirname, config.tmpl),
    extname:'xtpl'
}))

app.use(koaStatic(path.resolve(__dirname,'assert')))
app.use(koaStatic(path.resolve(__dirname,'../client/')))
app.use(koaBody())
app.use(router(app))

app.listen(3000)
//module.exports = app