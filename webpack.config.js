var webpack = require('webpack')
var path = require('path')
module.exports = {
    entry:__dirname + '/client/src/index.js',
    output: {
        path: __dirname + '/client/dist',
        filename:'index.js'
    },
    module:{
        rules:[{
            test:/\.js$/,
            use:{
                loader:'babel-loader',
            },
            exclude:/node_modules/
        },{
            test:/\.vod$/,
            use:{
                loader:path.resolve(__dirname,'loaders/vodloader/index.js'),
            }
        }]
    },
    devtool: "source-map",
    externals: {
        'jquery':'window.jQuery',
        'jsPlumb':'window.jsPlumb'
    }
}