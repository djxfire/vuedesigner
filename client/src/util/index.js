import File from '../cls/File'
import Procedure from '../cls/Procedure'
export function isVar(_var) {
    let re = /\{\{[\s\S]*\}\}/
    return re.test(_var)
}
export function randstr() {
    return Math.random().toString(36).substr(2)
}

export function camel2kabab(str) {
    return str.replace(/[A-Z]/g,(i) => {
        return '-' + i.toLowerCase()
    })
}
export function parseFile(desc,type) {
    let file = null
    if(type == 1){
        file = new Procedure(desc.name, desc.type,null)
    }else {
        file = new File(desc.name, desc.type,null)
    }
    file.content = desc.content
    file.originContent = desc.originContent
    let childs_arr = []
    desc.childs.forEach(val => {
        let file0 = parseFile(val,1)
        file0.parent = this
        childs_arr.push(file0)
    })
    file.childs = new Set(childs_arr)
    return file
}