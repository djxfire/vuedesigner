/**
 * 弹出对话框
 */

export default class Dialog {
    constructor(config) {
        let dialog = document.createElement("div")
        dialog.classList = "od-dialog"
        if(config.style) {
            for(let kk in config.style){
                dialog.style[kk] = config.style[kk]
            }
        }else {
            dialog.style.margin = "auto"
            dialog.style.width = "480px"
            dialog.style.height = "360px"
            dialog.style.left = 0
            dialog.style.right = 0
            dialog.style.top = 0
            dialog.style.bottom = 0
        }
        let dialogHeader = document.createElement("div")
        dialogHeader.classList = "od-dialog-header"
        let closeBtn = document.createElement("div")
        closeBtn.classList = "close"
        closeBtn.innerText = "×"
        dialog.appendChild(dialogHeader)

        if(config.title) {
            dialogHeader.innerText = config.title
        }
        dialogHeader.appendChild(closeBtn)
        closeBtn.addEventListener("click",function(e){
            dialog.parentNode.removeChild(dialog)
        })
        let content = document.createElement("div")
        content.classList = "od-dialog-content"
        content.innerHTML = config.template
        dialog.appendChild(content)
        this.dialog = dialog
    }
    show(){
        document.body.appendChild(this.dialog)
    }
    close() {
        if(this.dialog.parentNode)
            this.dialog.parentNode.removeChild(this.dialog)
    }
}