import Layout from '../components/Layout.vod'
import Style from '../components/Style.vod'
import Text from '../components/Text.vod'
import Link from '../components/Link.vod'
import HighLight from '../components/HighLight.vod'
import Horizontal from '../components/Horizontal.vod'
import Div from '../components/Div.vod'
import Badge from '../components/Badge.vod'
import Button from '../components/Button.vod'
import CellGroup from '../components/CellGroup.vod'
import Cell from '../components/Cell.vod'
import Circle from '../components/Circle.vod'
import Collapse from '../components/Collapse.vod'
import Icon from '../components/Icon.vod'
import List from '../components/List.vod'
import Loading from '../components/Loading.vod'
import NavBar from '../components/NavBar.vod'
import NoticeBar from '../components/NoticeBar.vod'
import Pagination from '../components/Pagination.vod'
import Panel from '../components/Panel.vod'
import Popup from '../components/Popup.vod'
import Progress from '../components/Progress.vod'
import Steps from '../components/Steps.vod'
import Step from '../components/Step.vod'
import Swipe from '../components/Swipe.vod'
import Tabs from '../components/Tabs.vod'
import Tab from '../components/Tab.vod'
import Tabbar from '../components/Tabbar.vod'
import Tag from '../components/Tag.vod'
import Checkbox from '../components/Checkbox.vod'
import Field from '../components/Field.vod'
import NumberKeyboard from '../components/NumberKeyboard.vod'
import PasswordInput from '../components/PasswordInput.vod'
import Radio from '../components/Radio.vod'
import Rate from '../components/Rate.vod'
import Search from '../components/Search.vod'
import Slider from '../components/Slider.vod'
import Stepper from '../components/Stepper.vod'
import Switch from '../components/Switch.vod'
import Uploader from '../components/Uploader.vod'
import DatetimePicker from '../components/DatetimePicker.vod'
import Picker from '../components/Picker.vod'
import PullRefresh from '../components/PullRefresh.vod'
import CollapseItem from '../components/CollapseItem.vod'
import Image from '../components/Image.vod'
import SwipeItem from '../components/SwipeItem.vod'
import BadgeGroup from '../components/BadgeGroup.vod'

let od = {
    badgegroup:BadgeGroup,
    swipeitem:SwipeItem,
    image:Image,
    collapseitem:CollapseItem,
    button:Button,
    cellgroup:CellGroup,
    cell:Cell,
    circle:Circle,
    collapse:Collapse,
    icon:Icon,
    list:List,
    loading:Loading,
    navbar:NavBar,
    noticebar:NoticeBar,
    pagination:Pagination,
    panel:Panel,
    popup:Popup,
    progress:Progress,
    step:Step,
    steps:Steps,
    swipe:Swipe,
    tabs:Tabs,
    tab:Tab,
    tabbar:Tabbar,
    tag:Tag,
    checkbox:Checkbox,
    field:Field,
    numberkeyboard:NumberKeyboard,
    passwordinput:PasswordInput,
    radio:Radio,
    rate:Rate,
    search:Search,
    slider:Slider,
    stepper:Stepper,
    switch:Switch,
    uploader:Uploader,
    datetimepicker:DatetimePicker,
    picker:Picker,
    pullrefresh:PullRefresh,
    layout:Layout,
    style:Style,
    text:Text,
    link:Link,
    highlight:HighLight,
    horizontal:Horizontal,
    div:Div,
    badge:Badge,
}

export default od