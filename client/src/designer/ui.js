import $ from 'jquery'
import od from './components'
import Dialog from '../widget/Dialog'
export function get_attr_menu(_cur) {
    if(!_cur.menu) {
        return ''
    }
    let self_menu = `<div target-view = '${_cur.name}'>${_cur.menu.template}</div>`
    if(_cur.menu.extends == undefined) {
        return self_menu
    }
    if(od[_cur.menu.extends] == undefined) {
        return self_menu
    }
    let _p = od[_cur.menu.extends]
    return self_menu + get_attr_menu(_p)
}
export function init_action_view() {
    $("[action-view]").each((index, item) => {
        let _self = $(item);
        let view = _self.attr("action-view")
        let div = $("<div></div>")
        div.attr("class","vm-view")
        div.html(od[view].templates[0].template)
        _self.append(div)
    })
}

export function init_tabs_view(_callback) {
    $(".od-tabs").on("click",".od-tab-title",(e)=>{
        let $self = $(e.target)
        $self.parent().children(".od-tab-title").removeClass("active")
        $self.addClass("active")
        let $content = $self.parent().next()
        let target = $self.attr("tab-target")
        $content.find(".od-tab-content-item").removeClass("active")
        $content.find("#" + target + ".od-tab-content-item").addClass("active")
        _callback(e)
    })
}

export function menu_panel(style, od_v, ele) {
    //构建menu菜单
    let menus = get_attr_menu(od_v)
    let props = od_v.props
    let events = od_v.events
    //对选中的值进行初始化
    let template =''
    if(props !== undefined && props !== "") {
        template += `<fieldset><legend>属性</legend>${props}</fieldset>`
    }
    if(events !== undefined && events !== "") {
        template += ` <fieldset><legend>事件</legend>${events}</fieldset>    `
    }
    if(menus !== undefined && menus !== "") {
        template += `<fieldset class = "style"><legend>操作</legend>${menus}</fieldset>`
    }
    //属性弹出菜单
    let dialog = new Dialog({
        title:"属性",
        style:style,
        template:template
    })
    dialog.show()
}
export function switch_page(project) {
    //添加当前的数据
    let dataFragment = document.createDocumentFragment()
    for(let key in project.manager.current.data){
        let dv = document.createElement("DIV")
        dv.classList = "data-box-item"
        let label_dv = document.createElement("DIV")
        let value_dv = document.createElement("DIV")
        label_dv.classList = "data-box-label"
        value_dv.classList = "data-box-value"
        let label_input = document.createElement("INPUT")
        let value_input = document.createElement("INPUT")
        label_input.type = "text"
        label_input.placeholder = "变量名"
        value_input.type = "text"
        value_input.placeholder = "变量值"
        label_input.value = key
        value_input.value = JSON.stringify(project.manager.current.data[key])
        label_dv.appendChild(label_input)
        value_dv.appendChild(value_input)
        dv.appendChild(label_dv)
        dv.appendChild(value_dv)
        dataFragment.appendChild(dv)
    }
    let vuedata = document.getElementById("vuedata")
    vuedata.innerHTML = ''
    vuedata.appendChild(dataFragment)
    $("#editor").html(project.manager.current.file.content)
}

