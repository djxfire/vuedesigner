import Project from '../cls/Project'
export function ins(proj,name){
    if(proj != undefined && proj != null) {
        proj = JSON.parse(proj)
        let project = Project.build(proj)
        project.manager.toView()
        return project
    }else {
        let project = new Project(name)
        project.init(document.getElementById('filelist'))
        return project
    }
}
