/**
 * 文件
 */
//import Procedure from './Procedure'
export default class File{
    /**
     * name:文件名
     * type:文件类型，0：视图文件，1：流程文件
     * parent:父文件
     * */
    constructor(name, type, parent = null) {
        this.name = name
        this.type = type
        this.parent = parent
        this.content = ""
        this.originContent = ""
        this.childs = new Set()
        if(this.parent !== null) {
            this.parent.addChild(this)
        }
    }
    static build(desc,type){
        let file = null
        if(type == 1){
        //    file = new Procedure(desc.name, desc.type,null)
        }else {
            file = new File(desc.name, desc.type,null)
        }
        file.content = desc.content
        file.originContent = desc.originContent
        let childs_arr = []
        desc.childs.forEach(val => {
            let file0 = File.build(val,1)
            file0.parent = this
            childs_arr.push(file0)
        })
        file.childs = new Set(childs_arr)
        return file
    }
    toJson(){
        let child_arr = [];
        [...this.childs].forEach(val => {
            child_arr.push(val.toJson())
        })
        return {
            name:this.name,
            type:this.type,
            parent:this.parent === null?{}:this.parent.toJson(),
            content:this.content,
            originContent:this.originContent,
            childs:child_arr,
        }
    }
    addChild(file) {
        this.childs.add(file)
        file.parent = this
    }
    removeChild(file) {
        this.childs.delete(file)
    }
    write(content){
        this.content = content
        this.resolve()
    }
    read() {
        return this.content
    }
    resolve() {
        let tmd = document.createElement("div")
        let dv = $(tmd)
        dv.html(this.content)
        dv.find(".preview").remove()
        dv.find(".drag").remove()
        dv.find(".vm-view").each((i,e) => {
            $(e).parent().append($(e).children())
            $(e).remove()
        })
        dv.find(".draggable").each((i,e) => {
            $(e).parent().append($(e).children())
            $(e).remove()
        })
        dv.find("[contenteditable]").each((i,e) => {
            $(e).removeAttr("contenteditable")
        })
        this.originContent = dv.html()
    }
    rename(name) {
        this.name = name
    }
    toView(isActive) {
        let html = ""
        if(isActive) {
            html += '<li class = "active" data-name="' + this.name + '">' + (this.type == 0?'<i class = "fa fa-file-o"></i>':(this.type == 1?'<i class = "fa fa-chain"></i>':'')) + this.name
        }else {
            html += '<li data-name="' + this.name + '">' + (this.type == 0?'<i class = "fa fa-file-o"></i>':(this.type == 1?'<i class = "fa fa-chain"></i>':'')) + this.name
        }
        if(this.childs.size > 0) {
            html += '<ul class = "sub-list" data-file-view="' + this.name + '">'
            for(let child of this.childs) {
                html += child.toView(false)
            }
            html += "</ul>"
        }
        html += "</li>"
        return html
    }
    getChild(filename) {
        for(let child of this.childs) {
            if(child.name == filename) {
                return child
            }
        }
    }
}