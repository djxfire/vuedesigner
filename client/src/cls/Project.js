/**
 * 工程类
 */
import File from './File'
import FileManager from './FileManager'
import Page from './Page'
import Procedure from "./Procedure";
import Od from './Od'
export default class Project {
    constructor(name, manager = null){
        this.name = name
        if(manager == null) {
            this.manager = new FileManager(null)
        }else {
            this.manager = manager
        }
        this.od_map = new Map()
        this.procedure_file = null  //当前选中的流程文件
        this.cur_procedure = null   //当前选中的流程节点
        this.cur_dom = null     //当前选中的元素节点
        this.cur_od = null      //当前选中的Od节点
        this.scene = 'view'     //当前操作的场景
    }
    init(ele) {
        let file = new File('index',0)
        let page = new Page('index',file,{})
        this.manager.ele = ele
        this.manager.add(page)
        this.manager.switch('index',()=>{})
        this.manager.toView(ele)
    }

    solve() {
        //TODO:工程解析成Vue单页面
        let pages = this.manager.pages
        let components = ''
        let routes = 'let router = ['
        for(let page of pages) {
            components += 'let ' + page.name + " = " + JSON.stringify(page.toView()) + '\n'
            routes += '{path:\'/' + page.name + '\',component:' + this.name + '},'
        }
        routes += ']\n'
        let ret = components + routes
        ret += 'let router = new VueRouter({routes:routes})\n'
        ret += 'let app = new Vue({router:router}).$mount(\'#app\')'
        return ret
    }
    save() {

    }
    toJson() {
        let od_map_arr = [];
        [...this.od_map].forEach((val) => {
            od_map_arr.push([val[0],val[1].toJson()])
        })
        return {
            name: this.name,
            manager:this.manager.toJson(),
            od_map: od_map_arr,
            procedure_file:this.procedure_file === null?{}:this.procedure_file.toJson(),
            cur_procedure: '',
            cur_dom: '',
            cur_od: '',
            scene: this.scene
        }
    }
    static build(obj) {
        let od_map_arr = []
        obj.od_map.forEach(val=>{
            od_map_arr.push([val[0],Od.parseJson(val[1])])
        })
        let project = new Project(obj.name)
        project.manager = FileManager.build(obj.manager)
        project.od_map = new Map(od_map_arr)
        project.procedure_file = null
        project.cur_procedure = null
        project.cur_dom = null //description.cur_dom
        project.cur_od = null //Od.parseJson(description.cur_od)
        project.scene = obj.scene
        return project
    }
}