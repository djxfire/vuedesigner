/**
 * 组件状态的数据结构
 */
export default class Od{
    constructor(
        id,/*当前在组件中DOM的ID，随机字符串*/
        name,/*当前模板的name属性*/
        ctrl,/*组件的类型，Ctrl对象*/
        origin,/*当前的Vue组件原始描述，String*/
        props = new Map(),/*当前DOM所有菜单的属性值，Map类型*/
    ) {
        this.id = id
        this.ctrl = ctrl
        this.origin = origin
        this.props = props
        this.name = name

    }
    toJson() {
        return {
            id:this.id,
            name:this.name,
            ctrl:this.ctrl,
            origin:this.origin.attr('outterHTML'),
            props:[...this.props]
        }
    }
    static parseJson(desc){
        return new Od(desc.id,desc.name,desc.ctrl,$(desc.origin),new Map(desc.props))
    }
    setOds(ods){
        this.ods = ods
    }
    /*
    计算当前DOM变更与origin的差异，并更新origin
    在定义组件被放置入容器时触发，向上所有组件容器
     */
    diff(){
        let target = $("#" + this.id).prop("outerHTML")//比较的目标对象
        let $target = $(target)
        let $my = this.origin
        //获取所有的slot,进行赋值
        let myslots = $my.find(".slot[target-view=" + this.id  + "]")
        let targetslots = $target.find(".slot[target-view=" + this.id + "]")
        console.log(target)
        console.log("my:",myslots,"target:",targetslots)
        for(let i = 0;i < myslots.length;i++) {
            $(myslots[i]).html($(targetslots[i]).html())
        }
    }
    build($ele) {
        let $my  = this.origin
        //:TODO添加props
        for(let _ of this.props.keys()) {
            let prop = this.props.get(_)
            //移除原先的绑定
            let target = $my.find(prop.target)
            let _key = _.replace(":","")
            target.removeAttr(_key)
            target.removeAttr(":" + _key)
            target.attr(_, prop.value)
        }
        console.log($my.children(".vm-view").html())
        $ele.children(".vm-view").html($my.children(".vm-view").html())

    }
    setProp(key, val,target) {
        //移除原先的绑定
        let _key = key.replace(":","")
        this.props.delete(_key)
        this.props.delete(":" + _key)
        this.props.set(key,{target:target,value:val})
    }
    getProps() {
        return this.props
    }
}