class FlowNode {
    constructor(id, type, content) {
        this.id = id
        this.type = type
        this.content = content
        this.attributes = {}
        this.spans = []
        this.left = 0
        this.top = 0
    }
    setPosition(left, top) {
        this.left = left
        this.top = top
    }
    setAttr(key, val) {
        this.attributes[key] = val
    }
    getAttr(key) {
        return this.attributes[key]
    }
    addSpan(target, agg) {
        this.spans.push({
            target:target,
            agg:agg
        })
    }
    hasSpan(target, agg) {
        for(let span of this.spans) {
            if(span.target === target && span.agg === agg) {
                return true
            }
        }
        return false
    }
}
class FlowTree {
    constructor() {
        this.nodes = []
    }
    get(id) {
        for(let node of this.nodes) {
            if(node.id === id) {
                return node
            }
        }
        return false
    }
    addNode(node) {
        this.nodes.push(node)
    }
}

export {FlowNode, FlowTree}