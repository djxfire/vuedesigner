//页面类，保存Vue页面信息

import {parseFile} from "../util"

export default class Page {
    constructor(name,file,data = {}){
        this.name = name
        this.file = file
        this.data = data
    }
    setData(key,val){
        this.data[key] = val
    }
    mkView() {
        let that = this
        let components = `{
            name: '${this.name}',
            data: 
                ${JSON.stringify(this.data)}
            ,
            methods:{`;
        for(let _ of this.file.childs) {
            components += _.name + _.content + ","
        }
        components += "}}"
        return new Function( "return " + components)()
    }
    toView(){
        let that = this
        let components = `{
            name: '${this.name}',
            template:'${this.file.originContent}',
            data(){
                return ${JSON.stringify(this.data)}
            },
            methods:{`;
        for(let _ of this.file.childs) {
            components += _.name + _.content + ","
        }
        components += "}}"
        console.log(components)
        //let fn = new Function( "return " + components)
        return ''
    }
    toJson(){
        return {
            name:this.name,
            file:this.file === null?{}:this.file.toJson(),
            data:this.data
        }
    }
    static build(desc) {
        return new Page(desc.name, parseFile(desc.file),desc.data)
    }
}