import {init_action_view,init_tabs_view} from "./designer/ui"
import jsPlumb from 'jsPlumb'
import {ins} from './designer/project'
import {init_ui_sortable} from './designer/builder'
import {init_procedure_view} from './designer/procedure'
import {onNewClick,onFileChange,onProcedureSave,onComponentClick, onViewContentChange,onKeyDown,onVueDataChange,onMenuClick} from './designer/event'
import axios from 'axios'
init_action_view()              //加载组件
init_tabs_view((e)=>{           //UITab初始化
    jsPlumb.repaintEverything()
})
axios.get('/project/init')
    .then(res => {
        if(res.data.code === 0) {
            let project_json = res.data.project.origin
            let project =  ins(project_json, res.data.project.name)            //实例化组件,Todo:从服务器获取工程数据
            console.log(project)
            $('#projectinfo').html(res.data.project.name)
            init_ui_sortable(project)       //初始化视图组件拖放
            init_procedure_view(project)    //初始化流程组件拖放
            onKeyDown(project)              //监听键盘事件
            onNewClick(project)             //监听新建事件
            onFileChange(project)           //监听文件管理器切换事件
            onProcedureSave(project)        //监听流程保存事件
            onComponentClick(project)       //监听组件选择事件
            onViewContentChange(project)    //监听组件内容修改事件
            onVueDataChange(project)        //监听组件数据修改事件
            onMenuClick(project)            //监听导航栏事件
        }
    }).catch(err => {
        console.log(err)
})




