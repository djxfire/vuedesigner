const path = require("path")
const templatesRE = /<templates>([\s\S]*)<\/templates>/
const templateRE = /<template\s*(name\s*=\s*"[\w]*")?\s*>((?!<\/template>).)*<\/template>/g
const template1RE = /<template\s*(name\s*=\s*"([\w]*)")?\s*>(((?!<\/template>).)*)<\/template>/
const slotsRE = /<slots>([\s\S]*)<\/slots>/
const slotRE = /<slot\s*(name\s*=\s*"[\w]*")?\s*>((?!<\/slot>).)*<\/slot>/g
const slot1RE = /<slot\s*(name\s*=\s*"[\w]*")?\s*>(((?!<\/slot>).)*)<\/slot>/
const propsRE = /<props>([\s\S]*)<\/props>/
const eventsRE = /<events>([\s\S]*)<\/events>/
const menuRE = /<menu\s*(extends\s*=\s*"([\w]*)")?\s*>([\s\S]*)<\/menu>/
const scriptsRE = /<script>([\s\S]*)<\/script>/
const splitRE = /\r?\n/g
const exportRE = /[\s\t]*export\s+default\s*\{$/
const openRE = /(\{)/g
const closeRE = /(\})/g
module.exports = function (source) {
    let templates = source.match(templatesRE)
    let scripts = source.match(scriptsRE)
    let props = source.match(propsRE)
    let events = source.match(eventsRE)
    let slots = source.match(slotsRE)
    if(templates === null || scripts === null) {
        throw '模板或脚本格式错误'
    }
    let [ret, scr, oth] = parseScripts(scripts[1])
    let tmp = parseTemplates(templates[1])
    if(slots != null) {
        let slot = parseSlots(slots[1])
        scr.unshift('slots:' + slot + ',')
    }


    scr.unshift('templates:' + tmp + ',')
    if(props !== null) {
        let p = parseProps(props[1])
        scr.unshift('props:\'' + relu_str(p) + '\',' )
    }
    if(events !== null) {
        let e = parseEvents(events[1])
        scr.unshift('events:\'' + relu_str(e) + '\',')
    }
    if(source.match(menuRE) !== null) {
        let mm = parseMenu(source)
        scr.unshift('menu:' + relu_str(mm) + ',')
    }
    scr.unshift('export default{\n')
    return ret.concat(scr.concat(oth)).join('\n')
}

function relu(str) {
    return str.replace(/\r/g, '').replace(/\t/g, '').replace(/\n/g, '')
}
function relu_str(str) {
    return str.replace(/\r?\t?\n?/g,'').replace(/\'/g,'\\\'')
}
function parseSlots(str) {
    str = relu(str)
    let tmps = str.match(slotRE)
    if(tmps === null) {
        console.log(str)
        throw '格式错误'

    }
    let tmp_arr = []
    tmps.forEach((_val) => {

        let tmp = {}
        let tt = _val.match(slot1RE)
        if(tt === null) {
            throw '格式错误'
        }
        tmp.name = tt[2]
        tmp.template = tt[3]
        tmp_arr.push(tmp)
    })
    return JSON.stringify(tmp_arr)
}
function parseTemplates(str) {
    str = relu(str)
    let tmps = str.match(templateRE)
    if(tmps === null) {
        console.log(str)
        throw '模板格式错误'

    }
    let tmp_arr = []
    tmps.forEach((_val) => {
        let tmp = {}
        let tt = _val.match(template1RE)
        if(tt === null) {
            throw '模板格式错误'
        }
        tmp.name = tt[2]
        tmp.template = tt[3]
        tmp_arr.push(tmp)
    })
    return JSON.stringify(tmp_arr)
}

function parseProps(str) {
    return relu(str)
}

function parseEvents(str) {
    return relu(str)
}

function parseMenu(str) {
    str = relu(str)
    let menu = str.match(menuRE)
    let obj = {}
    obj.extends = menu[2]
    obj.template = menu[3]
    return JSON.stringify(obj)
}

function parseScripts(str) {
    let lines = str.split(splitRE)
    let ret = [],scr = [],oth = []
    let flag = false,close = 0
    let name = null
    lines.forEach((line,index) => {
        exportRE.test(line)
            ?( flag=true,close++)
            :(flag === true && close === 0)
            ?oth.push(line)
            : (flag === true)
                ?(close += (line.match(openRE)?line.match(openRE).length:0) - (line.match(closeRE)?line.match(closeRE).length:0),
                        scr.push(line)
                )
                :ret.push(line)
    })
    return [
        ret,
        scr,
        oth
    ]

}